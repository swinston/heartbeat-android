package ai.fritz.heartbeat.env;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class, Logger.class})
public class LoggerTest {
    private Logger logger = null;

    @Before
    public void setup() {
        logger = new Logger("Tek", "unit-test");
        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void setMinLogLevel() {
        int minLogLevl = Whitebox.getInternalState(logger, "minLogLevel");
        assertEquals(minLogLevl, Log.DEBUG);
        logger.setMinLogLevel(Log.INFO);
        minLogLevl = Whitebox.getInternalState(logger, "minLogLevel");
        assertEquals(minLogLevl, Log.INFO);
    }

    @Test
    public void isLoggable() {
        assertTrue(logger.isLoggable(Log.ERROR));
        logger.setMinLogLevel(Log.ERROR);
        assertFalse(logger.isLoggable(Log.DEBUG));
    }

    @Test
    public void v() {
        logger.v("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.isLoggable(any(), any(Integer.class));
        verifyStatic(Log.class, Mockito.times(0));
        Log.v(any(), any());
        logger.setMinLogLevel(Log.VERBOSE);
        logger.v("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.isLoggable(any(), any(Integer.class));
        verifyStatic(Log.class, Mockito.times(1));
        Log.v(any(), any());
    }

    @Test
    public void v1() {
        Throwable throwable = new NullPointerException();
        logger.v(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.isLoggable(any(), any(Integer.class));
        verifyStatic(Log.class, Mockito.times(0));
        Log.v(any(), any(), any(Throwable.class));
        logger.setMinLogLevel(Log.VERBOSE);
        logger.v(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.isLoggable(any(), any(Integer.class));
        verifyStatic(Log.class, Mockito.times(1));
        Log.v(any(), any(), any(Throwable.class));
    }

    @Test
    public void d() {
        logger.d("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.d(any(), any());
    }

    @Test
    public void d1() {
        Throwable throwable = new NullPointerException();
        logger.d(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.d(any(), any(), any(Throwable.class));
    }

    @Test
    public void i() {
        logger.i("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.i(any(), any());
    }

    @Test
    public void i1() {
        Throwable throwable = new NullPointerException();
        logger.i(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.i(any(), any(), any(Throwable.class));
    }

    @Test
    public void w() {
        logger.w("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.w(any(), any(String.class));
    }

    @Test
    public void w1() {
        Throwable throwable = new NullPointerException();
        logger.w(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.w(any(), any(), any(Throwable.class));
    }

    @Test
    public void e() {
        logger.e("testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.e(any(), any(String.class));
    }

    @Test
    public void e1() {
        Throwable throwable = new NullPointerException();
        logger.e(throwable, "testFmt");
        verifyStatic(Log.class, Mockito.times(1));
        Log.e(any(), any(), any(Throwable.class));
    }
}