package ai.fritz.heartbeat.utils

import ai.fritz.heartbeat.*
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.GrantPermissionRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class NavigationTest {
    @Rule
    @JvmField
    val rule  = ActivityScenarioRule(MainActivity::class.java)

    @Rule
    @JvmField
    val intentRule = IntentsTestRule(MainActivity::class.java)

    @Rule @JvmField
    val grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.INTERNET
            )

    @Test
    fun goToLiveVideoFritzLabel() {
        intending(hasComponent(LiveVideoActivity::class.java.canonicalName))
        rule.scenario.onActivity {
            Navigation.goToLiveVideoFritzLabel(it)
        }
        intended(hasComponent(LiveVideoActivity::class.java.canonicalName))
    }

    @Test
    fun goToObjectDetection() {
        intending(hasComponent(DetectorActivity::class.java.canonicalName))
        rule.scenario.onActivity {
            Navigation.goToObjectDetection(it)
        }
        intended(hasComponent(DetectorActivity::class.java.canonicalName))
    }

    @Test
    fun goToStyleTransfer() {
        intending(hasComponent(StyleActivity::class.java.canonicalName))
        rule.scenario.onActivity {
            Navigation.goToStyleTransfer(it)
        }
        intended(hasComponent(StyleActivity::class.java.canonicalName))
    }

    @Test
    fun goToImageSegmentation() {
        intending(hasComponent(ImageSegmentationActivity::class.java.canonicalName))
        rule.scenario.onActivity {
            Navigation.goToImageSegmentation(it)
        }
        intended(hasComponent(ImageSegmentationActivity::class.java.canonicalName))
    }
}