package ai.fritz.vision.predictors

import ai.fritz.fritzvisionobjectmodel.ObjectDetectionOnDeviceModel
import ai.fritz.fritzvisionstylepaintings.PaintingStyles
import ai.fritz.heartbeat.MainActivity
import ai.fritz.vision.FritzVision
import ai.fritz.vision.FritzVisionImage
import ai.fritz.vision.imagelabeling.FritzVisionLabelPredictorOptions
import ai.fritz.vision.objectdetection.FritzVisionObjectPredictorOptions
import ai.fritz.visionlabel.ImageLabelOnDeviceModel
import android.graphics.BitmapFactory
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FritzVisionMLTest {
    var testImage: FritzVisionImage? = null
    @Rule
    @JvmField
    val rule  = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun Setup() {
        val file = FritzVisionMLTest::class.java.getResourceAsStream("/dog.jpeg")
        val image = BitmapFactory.decodeStream(file)
        testImage = FritzVisionImage.fromBitmap(image)
    }

    @Test
    fun RunLabelPredictionTest() {
        val options = FritzVisionLabelPredictorOptions.Builder()
                .confidenceThreshold(.1f).build()
        val onDeviceModel = ImageLabelOnDeviceModel()
        val predictor = FritzVision.ImageLabeling.getPredictor(onDeviceModel, options)
        val result = predictor!!.predict(testImage)
        val found = result.visionLabels.any { label ->
            label.text.toLowerCase().equals("dog")
        }
        assertTrue(found)
    }

    @Test
    fun RunRegionDetectPredictionTest() {
        val options = FritzVisionObjectPredictorOptions.Builder()
                .confidenceThreshold(.6f).build()
        val onDeviceModel = ObjectDetectionOnDeviceModel()
        val predictor = FritzVision.ObjectDetection.getPredictor(onDeviceModel, options)
        val results = predictor!!.predict(testImage)
        assertEquals(results.visionObjects.size, 1)
    }

    @Test
    fun RunStyleTransferPredictionTest() {
        val onDeviceModel = PaintingStyles.STARRY_NIGHT
        val predictor = FritzVision.StyleTransfer.getPredictor(onDeviceModel)
        val result = predictor!!.predict(testImage)
        assertFalse(result.resultBitmap.sameAs(testImage!!.bitmap))
    }
}